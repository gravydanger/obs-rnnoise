Unicode True
!define PRODUCT_NAME "obs-rnnoise"
!define PRODUCT_VERSION "0.1"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

SetCompressor /SOLID lzma

; MUI 1.67 compatible ------
!include "MUI.nsh"
!include "LogicLib.nsh"
!include "WordFunc.nsh"
!include "nsDialogs.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; Reserve files
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

; MUI end ------

Var has32
Var has64

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "build\obs-rnnoise-setup.exe"
;ShowInstDetails show
;ShowUnInstDetails show

Section "MainSection" SEC01
  SetDetailsPrint both
  DetailPrint "Installing files..."
  SetOverwrite ifnewer
  ${If} $has32 == "yes"
    DetailPrint "Installing 32-bit plugin"
    SetOutPath "$INSTDIR\obs-plugins\32bit"
    File "build\32bit\obs-rnnoise.dll"
  ${EndIf}
  ${If} $has64 == "yes"
    DetailPrint "Installing 64-bit plugin"
    SetOutPath "$INSTDIR\obs-plugins\64bit"
    File "build\64bit\obs-rnnoise.dll"
  ${EndIf}
  CreateDirectory "$SMPROGRAMS\obs-rnnoise"
SectionEnd

Section -AdditionalIcons
  SetOutPath $INSTDIR
  CreateShortCut "$SMPROGRAMS\obs-rnnoise\Uninstall.lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\obs-rnnoise-uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\obs-rnnoise-uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
SectionEnd


Function .onInit
  StrCpy $has32 "no"
  StrCpy $has64 "no"
  ReadRegStr $INSTDIR HKLM "Software\WOW6432Node\OBS Studio" ""
  StrCmp $INSTDIR "" 0 CheckExec
  ReadRegStr $INSTDIR HKLM "Software\OBS Studio" ""
  StrCmp $INSTDIR "" 0 CheckExec
  ReadRegStr $INSTDIR HKCU "Software\WOW6432Node\OBS Studio" ""
  StrCmp $INSTDIR "" 0 CheckExec
  ReadRegStr $INSTDIR HKCU "Software\OBS Studio" ""
  StrCmp $INSTDIR "" 0 CheckExec

  ${If} $INSTDIR == ""
    MessageBox MB_OK "OBS Studio install location not found. You can manually find it in the next step."
    nsDialogs::SelectFolderDialog "Choose the OBS Studio install location" "$PROGRAMFILES"
    Pop $INSTDIR
    ${If} $INSTDIR == ""
      MessageBox MB_OK "No OBS executable selected - aborting."
      Abort
    ${EndIf}
  ${EndIf}
  CheckExec:
  ${If} ${FileExists} "$INSTDIR\bin\32bit\obs32.exe"
    StrCpy $has32 "yes"
  ${EndIf}
  ${If} ${FileExists} "$INSTDIR\bin\64bit\obs64.exe"
    StrCpy $has64 "yes"
  ${EndIf}
  ${If} $has32 == "no"
  ${AndIf} $has64 == "no"
    MessageBox MB_OK "Couldn't find OBS executables in detected/selected location - aborting."
    Abort
  ${EndIf}
FunctionEnd

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  Delete "$INSTDIR\obs-rnnoise-uninst.exe"
  Delete "$INSTDIR\obs-plugins\32bit\obs-rnnoise.dll"
  Delete "$INSTDIR\obs-plugins\64bit\obs-rnnoise.dll"

  Delete "$SMPROGRAMS\obs-rnnoise\Uninstall.lnk"
  RMDir "$SMPROGRAMS\obs-rnnoise"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  SetAutoClose true
SectionEnd
