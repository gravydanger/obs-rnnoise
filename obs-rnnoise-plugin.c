#include <obs-module.h>

OBS_DECLARE_MODULE()
OBS_MODULE_USE_DEFAULT_LOCALE("obs-rnnoise", "en-US")

extern struct obs_source_info obsrnn_source;

bool obs_module_load(void)
{
	obs_register_source(&obsrnn_source);
	return true;
}
