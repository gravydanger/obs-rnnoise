#include <math.h>

#include <obs-module.h>
#include <util/circlebuf.h>
#include <rnnoise-nu.h>

#define SETTING_ATT "max_attenuation"
#define SETTING_ENV "env"
#define SETTING_NOISE_TYPE "noise_type"

#define FRAMESIZE 480
#define FRAMESIZE_BYTES (FRAMESIZE * sizeof(float))
#define SEC_TO_NSEC 1000000000ULL

#define FOR_CHANNELS(ctx) for (size_t ch = 0; ch < ctx->channels; ch++)

struct obsrnn_data {
	obs_source_t *context;
	int reset;
	char *rnn_model;
	DenoiseState *state[MAX_AV_PLANES];
	size_t channels;
	uint64_t last_ts;
	uint64_t samplerate;

	float max_attenuation;

	struct circlebuf info;
	struct circlebuf input[MAX_AV_PLANES];
	struct circlebuf output[MAX_AV_PLANES];
	struct obs_audio_data output_audio;
	DARRAY(float) output_data;
};

const char *obsrnn_name(void *unused)
{
	UNUSED_PARAMETER(unused);
	return obs_module_text("RNNoise noise remover (Xiph.Org)");
}

static void obsrnn_free_state(struct obsrnn_data *ctx)
{
	for (size_t ch = 0; ch < MAX_AV_PLANES; ch++) {
		if (!ctx->state[ch]) break;
		rnnoise_destroy(ctx->state[ch]);
		circlebuf_free(&ctx->input[ch]);
		circlebuf_free(&ctx->output[ch]);
		ctx->state[ch] = NULL;
	}
	circlebuf_free(&ctx->info);
}

void obsrnn_destroy(void *data)
{
	struct obsrnn_data *ctx = data;
	obsrnn_free_state(ctx);
	da_free(ctx->output_data);
	bfree(ctx);
}

static char *obsrnn_get_model(obs_data_t *s)
{
	const char *env = obs_data_get_string(s, SETTING_ENV);
	const char *noise_type = obs_data_get_string(s, SETTING_NOISE_TYPE);
	if (!strcmp(env, "general")) {
		if (!strcmp(noise_type, "general")) return "mp";
		if (!strcmp(noise_type, "voice")) return "lq";
		return "orig";
	} else {
		if (!strcmp(noise_type, "general")) return "cb";
		if (!strcmp(noise_type, "voice")) return "bd";
		return "sh";
	}
}

void obsrnn_update(void *data, obs_data_t *s)
{
	struct obsrnn_data *ctx = data;
	double val = obs_data_get_double(s, SETTING_ATT);
	size_t old_channels = ctx->channels;
	uint64_t old_samplerate = ctx->samplerate;
	char *old_rnn_model = ctx->rnn_model;

	ctx->samplerate = audio_output_get_sample_rate(obs_get_audio());
	ctx->channels = audio_output_get_channels(obs_get_audio());
	/* rnnoise-nu talks about a limit of -60 dB but they actually mean a multiplier of 10^(-60) */
	ctx->max_attenuation = powf(10.0, (float)val / 20.0);
	ctx->rnn_model = obsrnn_get_model(s);

	if (ctx->channels != old_channels || ctx->samplerate != old_samplerate) ctx->reset = 1;
	if (ctx->rnn_model != old_rnn_model) ctx->reset = 1;

}

void obsrnn_reset_data(struct obsrnn_data *ctx)
{
	FOR_CHANNELS(ctx) {
		circlebuf_pop_front(&ctx->input[ch], NULL, ctx->input[ch].size);
		circlebuf_pop_front(&ctx->output[ch], NULL, ctx->input[ch].size);
	}
	circlebuf_pop_front(&ctx->info, NULL, ctx->info.size);
}

void obsrnn_reinit(struct obsrnn_data *ctx)
{
	ctx->last_ts = 0;
	obsrnn_free_state(ctx);
	FOR_CHANNELS(ctx) {
		ctx->state[ch] = rnnoise_create(rnnoise_get_model(ctx->rnn_model));
		rnnoise_set_param(ctx->state[ch], RNNOISE_PARAM_MAX_ATTENUATION, ctx->max_attenuation);
		rnnoise_set_param(ctx->state[ch], RNNOISE_PARAM_SAMPLE_RATE, ctx->samplerate);
		circlebuf_reserve(&ctx->input[ch], FRAMESIZE_BYTES);
		circlebuf_reserve(&ctx->output[ch], FRAMESIZE_BYTES);
	}
	ctx->reset = 0;
}

void *obsrnn_create(obs_data_t *s, obs_source_t *filter)
{
	struct obsrnn_data *ctx = bzalloc(sizeof(*ctx));
	ctx->context = filter;
	obsrnn_update(ctx, s);
	return ctx;
}

static bool is_timestamp_jump(uint64_t ts, uint64_t prev_ts)
{
	return ts < prev_ts || (ts - prev_ts) > SEC_TO_NSEC;
}

void obsrnn_process_one(struct obsrnn_data *ctx)
{
	float rnn_buf[FRAMESIZE];
	FOR_CHANNELS(ctx) {
		circlebuf_pop_front(&ctx->input[ch], rnn_buf, FRAMESIZE_BYTES);
		rnnoise_set_param(ctx->state[ch], RNNOISE_PARAM_MAX_ATTENUATION, ctx->max_attenuation);
		/* RNNoise uses floats over a "16-bit" range */
		for (int i = 0; i < FRAMESIZE; i++) rnn_buf[i] *= 0x7fff;
		rnnoise_process_frame(ctx->state[ch], rnn_buf, rnn_buf);
		for (int i = 0; i < FRAMESIZE; i++) rnn_buf[i] /= 0x7fff;
		circlebuf_push_back(&ctx->output[ch], rnn_buf, FRAMESIZE_BYTES);
	}
}

struct obsrnn_audio_info {
	uint32_t frames;
	uint64_t timestamp;
};

struct obs_audio_data *obsrnn_filter_audio(void *data, struct obs_audio_data *audio)
{
	struct obsrnn_data *ctx = data;
	struct obsrnn_audio_info info;
	size_t out_size;

	if (ctx->reset) obsrnn_reinit(ctx);

	if (is_timestamp_jump(audio->timestamp, ctx->last_ts)) {
		obsrnn_reset_data(ctx);
	}
	ctx->last_ts = audio->timestamp;

	/* 1. Store expected frame count and timestamp for this packet */
	info.frames = audio->frames;
	info.timestamp = audio->timestamp;
	circlebuf_push_back(&ctx->info, &info, sizeof(info));

	/* 2. Add audio data to buffer */
	FOR_CHANNELS(ctx) circlebuf_push_back(&ctx->input[ch], audio->data[ch], audio->frames * sizeof(float));

	/* 3. Process as many RNNoise frames as we can */
	while (ctx->input[0].size >= FRAMESIZE_BYTES)
		obsrnn_process_one(ctx);

	/* 4. Check if we have enough data to fill the next packet OBS expects */
	circlebuf_peek_front(&ctx->info, &info, sizeof(info));
	out_size = info.frames * sizeof(float);
	if (ctx->output[0].size < out_size)
		return NULL;

	/* 5. We've got enough data, wrap it up */
	circlebuf_pop_front(&ctx->info, NULL, sizeof(info));
	da_resize(ctx->output_data, out_size * ctx->channels);
	FOR_CHANNELS(ctx) {
		ctx->output_audio.data[ch] = (uint8_t *)&ctx->output_data.array[ch * out_size];
		circlebuf_pop_front(&ctx->output[ch], ctx->output_audio.data[ch], out_size);
	}
	ctx->output_audio.frames = info.frames;
	ctx->output_audio.timestamp = info.timestamp;
	return &ctx->output_audio;
}

void obsrnn_defaults(obs_data_t *data)
{
	obs_data_set_default_double(data, SETTING_ATT, -120.0);
	obs_data_set_default_string(data, SETTING_ENV, "recording");
	obs_data_set_default_string(data, SETTING_NOISE_TYPE, "general");
}

obs_properties_t *obsrnn_properties(void *data)
{
	obs_properties_t *props = obs_properties_create();
	obs_properties_add_float_slider(
		props,
		SETTING_ATT,
		obs_module_text("Maximum noise attenuation (dB)"),
		-120.0, 0.0, 0.1);

	obs_property_t *list = obs_properties_add_list(
		props,
		SETTING_ENV,
		obs_module_text("Environment"),
		OBS_COMBO_TYPE_LIST,
		OBS_COMBO_FORMAT_STRING);
	obs_property_list_add_string(list, obs_module_text("Busy"), "general");
	obs_property_list_add_string(list, obs_module_text("Recording"), "recording");

	list = obs_properties_add_list(
		props,
		SETTING_NOISE_TYPE,
		obs_module_text("Type of noise to filter"),
		OBS_COMBO_TYPE_LIST,
		OBS_COMBO_FORMAT_STRING);
	obs_property_list_add_string(list, obs_module_text("General"), "general");
	obs_property_list_add_string(list, obs_module_text("Speech"), "speech");
	obs_property_list_add_string(list, obs_module_text("Voice (including e.g. laughter)"), "voice");

	UNUSED_PARAMETER(data);
	return props;
}

struct obs_source_info obsrnn_source = {
	.id = "rnnoise_filter",
	.type = OBS_SOURCE_TYPE_FILTER,
	.output_flags = OBS_SOURCE_AUDIO,
	.get_name = obsrnn_name,
	.create = obsrnn_create,
	.destroy = obsrnn_destroy,
	.update = obsrnn_update,
	.filter_audio = obsrnn_filter_audio,
	.get_defaults = obsrnn_defaults,
	.get_properties = obsrnn_properties,
};
