# Quick and dirty Makefile to build the plugin using Cygwin's mingw clang compilers
#
# Example:
# make build OBS_DIR="/cygdrive/c/Program Files (x86)/obs-studio"

export HOSTARCH := w64
# export HOSTARCH := pc
build: build/32bit build/64bit

build/32bit: build/32bit/Makefile
	$(MAKE) -C $@
build/64bit: build/64bit/Makefile
	$(MAKE) -C $@
build/32bit/Makefile: Makefile-build.in
	@sed -e 's/\%ARCH\%/i686/' -e 's/\%BITS\%/32/' $< > $@
build/64bit/Makefile: Makefile-build.in
	@sed -e 's/\%ARCH\%/x86_64/' -e 's/\%BITS\%/64/' $< > $@

clean:
	rm -rf build/32bit/*.o build/64bit/*.o build/32bit/.deps build/64bit/.deps build/32bit/obs-rnnoise.dll build/64bit/obs-rnnoise.dll

.PHONY: clean build build/32bit build/64bit
